package com.pikh.controller;

import com.pikh.model.appliance.ElectricalAppliance;

import java.util.List;

/**
 * Controller interface.
 */
public interface Controller {
    /**
     * get list of appliance
     * @return
     */
    List getApplianceList();

    /**
     * get appliance by id
     * @param id
     * @return
     */
    ElectricalAppliance getAppliance(int id);

    /**
     * turn on appliance
     * @param appliance
     */
    void turnOn(ElectricalAppliance appliance);

    /**
     * turn off appliance
     * @param appliance
     */
    void turnOf(ElectricalAppliance appliance);

    /**
     * get power consumption
     * @return
     */
    int getPowderConsumption();

    /**
     * find appliance by power
     * @param power
     * @return
     */
    ElectricalAppliance findByPower(int power);
}
