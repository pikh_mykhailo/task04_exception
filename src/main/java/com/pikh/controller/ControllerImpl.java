package com.pikh.controller;

import com.pikh.model.Model;
import com.pikh.model.appliance.ElectricalAppliance;

import java.util.List;

/**
 * Controller implementation.
 */
public class ControllerImpl implements Controller {

    /**
     * Model
     */
    private Model model;

    /**
     *
     * @param model
     */
    public ControllerImpl(Model model) {
        this.model = model;
    }

    @Override
    public List getApplianceList() {
        return model.getApplianceList();
    }

    @Override
    public ElectricalAppliance getAppliance(int id) {
        return model.getAppliance(id);
    }

    @Override
    public void turnOn(ElectricalAppliance appliance) {
        model.turnOn(appliance);
    }

    @Override
    public void turnOf(ElectricalAppliance appliance) {
        model.turnOf(appliance);
    }

    @Override
    public int getPowderConsumption() {
        return model.getPowderConsumption();
    }

    @Override
    public ElectricalAppliance findByPower(int power) {
        return model.findByPower(power);
    }
}

