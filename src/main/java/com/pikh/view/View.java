package com.pikh.view;

import com.pikh.controller.Controller;
import com.pikh.controller.ControllerImpl;
import com.pikh.model.Apartment;
import com.pikh.model.Service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * View class in MVC model.
 */
public class View {

    /**
     * Input  for data
     */
    private static Scanner input = new Scanner(System.in);

    /**
     * Controller in MVC model.
     */
    private Controller controller;

    /**
     * Menu that customer would see.
     */
    private Map<String, String> menu;

    /**
     * Map of methods.
     */
    private Map<String, Printable> methodsMenu;

    /**
     *Method that join methods with buttons from view model.
     */
    public View() {
        controller = new ControllerImpl(new Service(new Apartment()));
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print Appliance list");
        menu.put("2", "  2 - print some Appliance");
        menu.put("3", "  3 - turn on Appliance");
        menu.put("4", "  4 - turn of Appliance");
        menu.put("5", "  5 - print power consumption");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    /**
     * method that show list of appliance.
     */
    private void pressButton1() {
        System.out.println(controller.getApplianceList());
    }

    /**
     * method that find and show some appliance.
     */
    private void pressButton2() {
        System.out.println("Please input id:");
        int id = Integer.parseInt(input.nextLine());
        System.out.println(controller.getAppliance(id));
    }

    /**
     * turn on appliance
     */
    private void pressButton3() {
        System.out.print("Please input id of appliance to turn on:");
        int id = Integer.parseInt(input.nextLine());
        controller.turnOn(controller.getAppliance(id));
        System.out.println(controller.getAppliance(id));
    }

    /**
     * turn off appliance
     */
    private void pressButton4() {
        System.out.print("Please input id of appliance to turn of:");
        int id = Integer.parseInt(input.nextLine());
        controller.turnOf(controller.getAppliance(id));
        System.out.println(controller.getAppliance(id));
    }

    /**
     * show the powder consumption
     */
    private void pressButton5() {
        System.out.println(controller.getPowderConsumption());
    }

    /**
     * out menu to console
     */
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    /**
     *
     */
    public final void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
