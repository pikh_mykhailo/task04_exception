package com.pikh.view;

/**
 * Functional interface.
 */
@FunctionalInterface
public interface Printable {
     /**
      * abstract method that use in view.
      */
     void print();
}
