package com.pikh;

import com.pikh.view.View;

/**
 *Main class.
 */
public class Application {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        new View().show();
    }
}
