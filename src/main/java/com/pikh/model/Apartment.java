package com.pikh.model;

import com.pikh.model.appliance.ElectricalAppliance;
import com.pikh.model.appliance.NoteBook;
import com.pikh.model.appliance.Powder;
import com.pikh.model.appliance.Tv;

import java.util.ArrayList;
import java.util.List;

/**
 * Model class.
 */
public class Apartment {

    /**
     *Sum of power  enabled appliance
     */
    private int powerConsumption;

    /**
     * list of appliance in apartment
     */
    private List<ElectricalAppliance> appliances;

    /**
     * Constructor
     */
    public Apartment() {
        appliances = new ArrayList<>();
        fillApartment();
    }

    /**
     * Fill the apartment by appliances.
     */
    private void fillApartment() {
        appliances.add(new Tv(49, 1, "Samsung", false, 100, 7));
        appliances.add(new NoteBook(17, 2, "Dell", false, 250, 2));
        appliances.add(new Powder(4, 3, "Philips", false, 150, 4));
    }

    /**
     *Get list of appliances in apartment
     */
    public List getApplianceList() {
        return appliances;
    }

    /**
     *Get appliance by id
     * @param id
     * @return
     */
    public ElectricalAppliance getAppliance(int id) {
        return appliances.get(id - 1);
    }

    /**
     * Turn on appliance
     * @param appliance
     */
    public void turnOn(ElectricalAppliance appliance) {
        appliance.turnOn();
    }

    /**
     * Turn off appliance
     * @param appliance
     */
    public void turnOf(ElectricalAppliance appliance) {
        appliance.turnOf();
    }

    /**
     * Return sum power consumption
     * @return
     */
    public int getPowderConsumption() {
        powerConsumption = 0;
        for (ElectricalAppliance appliance : appliances) {
            powerConsumption += appliance.getPower();
        }
        return powerConsumption;
    }

    /**
     * Find some appliance by power
     * @param power
     * @return
     */
    public ElectricalAppliance findByPower(int power) {
        ElectricalAppliance electricalAppliance = null;
        for (ElectricalAppliance appliance : appliances) {
            if (appliance.getPower() == power) {
                electricalAppliance = appliance;
            }
        }
        return electricalAppliance;
    }
}
