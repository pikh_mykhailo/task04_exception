package com.pikh.model;

import com.pikh.model.appliance.ElectricalAppliance;

import java.util.List;

/**
 * Service in MVC model.
 */
public class Service implements Model {
    /**
     * Model in MVC
     */
    private Apartment apartment;

    /**
     * Constructor
     * @param apartment
     */
    public Service(Apartment apartment) {
        this.apartment = apartment;
    }

    @Override
    public List getApplianceList() {
        return apartment.getApplianceList();
    }

    @Override
    public ElectricalAppliance getAppliance(int id) {
        return apartment.getAppliance(id);
    }

    @Override
    public void turnOn(ElectricalAppliance appliance) {
        apartment.turnOn(appliance);
    }

    @Override
    public void turnOf(ElectricalAppliance appliance) {
        apartment.turnOf(appliance);
    }

    @Override
    public int getPowderConsumption() {
        return apartment.getPowderConsumption();
    }

    @Override
    public ElectricalAppliance findByPower(int power) {
        return apartment.findByPower(power);
    }
}
