package com.pikh.model.appliance;

/**
 * Parent class of appliance.
 */
public class ElectricalAppliance {
    /**
     * id
     */
    private int id;

    /**
     * power of appliance
     */
    private int power;

    /**
     * weight of appliance
     */
    private int weight;

    /**
     * brand of appliance
     */
    private String brand;

    /**
     * check is appliance turn on
     */
    private boolean isTurnOn;

    /**
     *
     * @param id
     * @param brand
     * @param isTurnOn
     * @param power
     * @param weight
     */
    public ElectricalAppliance(int id, String brand, boolean isTurnOn, int power, int weight) {
        this.id = id;
        this.brand = brand;
        this.isTurnOn = isTurnOn;
        this.power = power;
        this.weight = weight;
    }

    /**
     * turn on appliance
     */
    public void turnOn() {
        isTurnOn = true;
    }

    /**
     * turn of appliance
     */
    public void turnOf() {
        isTurnOn = false;
    }

    /**
     * id getter
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * id setter
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * brand getter
     * @return
     */
    public String getBrand() {
        return brand;
    }

    /**
     * brand setter
     * @param brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * is turn on getter
     * @return
     */
    public boolean isTurnOn() {
        return isTurnOn;
    }

    /**
     * is turn on setter
     * @param isTurnOn
     */
    public void setIsTurnOn(boolean isTurnOn) {
        this.isTurnOn = isTurnOn;
    }

    /**
     * power getter
     * @return
     */
    public int getPower() {
        return power;
    }

    /**
     * power setter
     * @param power
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * weight getter
     * @return
     */
    public int getWeight() {
        return weight;
    }

    /**
     * weight setter
     * @param weight
     */
    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "id = " + id +
                " brand='" + brand + '\'' +
                ", power=" + power +
                ", weight=" + weight +
                ", isTurnOn=" + isTurnOn +
                "}";
    }
}
