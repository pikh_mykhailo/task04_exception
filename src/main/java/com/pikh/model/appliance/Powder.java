package com.pikh.model.appliance;

/**
 * Created by Michail on 11/13/2019.
 */
public class Powder extends ElectricalAppliance {
    /**
     * Count of attachment
     */
    private int attachment;

    /**
     *
     * @param attachment
     * @param id
     * @param brand
     * @param isTurnOn
     * @param power
     * @param weight
     */
    public Powder(int attachment, int id, String brand, boolean isTurnOn, int power, int weight) {
        super(id, brand, isTurnOn, power, weight);
        this.attachment = attachment;
    }

    /**
     * Enable reverse work
     */
    public void reverse() {
        System.out.println("REVERSE WORK!!!");
    }

    /**
     * attachment getter
     * @return
     */
    public int getAttachment() {
        return attachment;
    }

    /**
     * attachment setter
     * @param attachment
     */
    public void setAttachment(int attachment) {
        this.attachment = attachment;
    }

    @Override
    public String toString() {
        return "Powder{" + super.toString() +
                "attachment=" + attachment +
                "}\n";
    }
}
