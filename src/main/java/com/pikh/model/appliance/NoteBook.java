package com.pikh.model.appliance;

/**
 * Created by Michail on 11/13/2019.
 */
public class NoteBook extends ElectricalAppliance {
    /**
     * Capacity of battery.
     */
    private int batteryCapacity;

    /**
     *
     * @param batteryCapacity
     * @param id
     * @param brand
     * @param isTurnOn
     * @param power
     * @param weight
     */
    public NoteBook(int batteryCapacity, int id, String brand, boolean isTurnOn, int power, int weight) {
        super(id, brand, isTurnOn, power, weight);
        this.batteryCapacity = batteryCapacity;
    }

    /**
     * Run browser in notebook
     */
    public void runBrowser() {
        System.out.println("RUN BROWSER!!!");
    }

    /**
     * battery capacity getter
     * @return
     */
    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    /**
     * * battery capacity setter
     * @param batteryCapacity
     */
    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    @Override
    public String toString() {
        return "NoteBook{" + super.toString() +
                "batteryCapacity=" + batteryCapacity +
                "}\n";
    }
}
