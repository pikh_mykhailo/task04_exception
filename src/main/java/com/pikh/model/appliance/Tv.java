package com.pikh.model.appliance;

/**
 * Created by Michail on 11/13/2019.
 */
public class Tv extends ElectricalAppliance {
    /**
     * Size of TV
     */
    private int size;

    /**
     *
     * @param size
     * @param id
     * @param brand
     * @param isTurnOn
     * @param power
     * @param weight
     */
    public Tv(int size, int id, String brand, boolean isTurnOn, int power, int weight) {
        super(id, brand, isTurnOn, power, weight);
        this.size = size;
    }

    /**
     * Connect antenna to TV;
     */
    public void connectAntenna() {
        System.out.println("Connect Antenna");
    }

    /**
     * Size getter
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * Size setter
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Tv{" + super.toString() +
                "size=" + size +
                "}\n";
    }
}
