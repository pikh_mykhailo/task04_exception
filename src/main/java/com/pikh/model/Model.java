package com.pikh.model;

import com.pikh.model.appliance.ElectricalAppliance;

import java.util.List;

/**
 * Interface.
 */
public interface Model {

    /**
     * List of appliance.
     * @return
     */
    List getApplianceList();

    /**
     * Get some appliance , input id.
     * @param id
     * @return
     */
    ElectricalAppliance getAppliance(int id);

    /**
     * Turn on some appliance.
     * @param appliance
     */
    void turnOn(ElectricalAppliance appliance);

    /**
     * Turn of some appliance.
     * @param appliance
     */
    void turnOf(ElectricalAppliance appliance);

    /**
     * Get sum of power consumption in apartment.
     * @return
     */
    int getPowderConsumption();

    /**
     * Find some appliance by power.
     * @param power
     * @return
     */
    ElectricalAppliance findByPower(int power);
}
